#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sndfile.h>

#include <math.h>
#include <complex.h>
#include <fftw3.h>
#include "gnuplot_i.h"


#define	FRAME_SIZE 1024
#define HOP_SIZE 512


#define TOTAL_SAMPLES 441000
/* 10 sec */

static gnuplot_ctrl *h;
static fftw_plan plan = NULL;

static fftw_plan plan2 = NULL;

static void
print_usage (char *progname)
{	printf ("\nUsage : %s <output file>\n", progname) ;
	puts ("\n"
		) ;

} 

static int
write_n_samples (SNDFILE * outfile, double * buffer, int channels, int n)
{
  if (channels == 1)
    {
      /* MONO */
      int writecount ;

      writecount = sf_writef_double (outfile, buffer, n);

      return writecount==n;
    }
  else
    {
      /* FORMAT ERROR */
      printf ("Channel format output error.\n");
    }
  
  return 0;
} 


static int
write_samples (SNDFILE * outfile, double * buffer, int channels)
{
  return write_n_samples (outfile, buffer, channels, HOP_SIZE);
}


void
fft_init (complex in[FRAME_SIZE], complex spec[FRAME_SIZE])
{
  plan = fftw_plan_dft_1d(FRAME_SIZE, in, spec, FFTW_FORWARD, FFTW_ESTIMATE);
  plan2 = fftw_plan_dft_1d(FRAME_SIZE,in,spec,FFTW_BACKWARD,FFTW_ESTIMATE);
}

void fft_exit (void)
{
  fftw_destroy_plan(plan);
  fftw_destroy_plan (plan2);
}

void
fft_process (void)
{
  fftw_execute(plan);
}

void
ifft_process (void) {
  fftw_execute(plan2);
}



int
main (int argc, char * argv [])
{	char 		*progname, *outfilename ;
  SNDFILE		*outfile = NULL ;
  SF_INFO	 	sfinfo = {0} ;

  sfinfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
  sfinfo.channels = 1;
  sfinfo.samplerate = 44100;
  
  progname = strrchr (argv [0], '/') ;
  progname = progname ? progname + 1 : argv [0] ;

  if (argc != 2)
    {	print_usage (progname) ;
      return 1 ;
    } ;

  outfilename = argv [1] ;

  /* Open the output file. */
  if ((outfile = sf_open (outfilename, SFM_WRITE, &sfinfo)) == NULL)
    {	printf ("Not able to open input file %s.\n", outfilename) ;
      puts (sf_strerror (NULL)) ;
      return 1 ;
    } ;
  
  double buffer[FRAME_SIZE];
  complex samples[FRAME_SIZE];
  double amplitude[FRAME_SIZE];
  double phase[FRAME_SIZE];
  complex spec[FRAME_SIZE];
  double x_axis[FRAME_SIZE];

  int nb_frames=0;
  int i = 0;
  
  /* Plot Init */
  h=gnuplot_init();
  gnuplot_setstyle(h, "lines");
  
  /* FFT init */
  fft_init(samples, spec);
  
  while (nb_frames*HOP_SIZE < TOTAL_SAMPLES)
    {
      /* Process Samples */
      //printf("Processing frame %d\n",nb_frames);

      /* SYNTHESE BRUIT*/
      
      
      // fft input
      for (i=0; i <FRAME_SIZE; i++)
	      samples[i] = buffer[i];
      
      fft_process();

      // calcul du spectre amplitude
      for (i=0; i <FRAME_SIZE; i++){
	      amplitude[i] = 1 ;
        phase[i] = carg(spec[i]);
      }

      for(int i = 0; i<FRAME_SIZE; i++) {
        spec[i] = amplitude[i] * cexp(phase[i]);
      }

      ifft_process();
      
      for (i=0;i<FRAME_SIZE;i++) {
        //printf("phase[%d] = %d\n",i,phase[i]);
    	  s[i] = creal(S1[i]);
      }
      /* SAVE */
      if (write_samples (outfile, buffer, sfinfo.channels)!=1)
	printf("saving problem !! \n");

      nb_frames++;
    }


  sf_close (outfile) ;

  /* FFT exit */
  fft_exit();

  return 0 ;
} /* main */


